// document - it refers to the whole web page
//querySelector - it is used to select specific objects (HTML Elements) from the document(webpage)
const txtFirstName = document.querySelector("#txt-first-name");
//const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// document.getElementById
// document.getElementByClass
// document.getElementByTagName
// const txtFirstName = document.getElementById("txt-first-name");
// const spanFullName = document.getElementById("span-full-name");

//whenever a user interacts with a webpage, this action is considered as a event
//addEventListener - function that takes 2 arguments
//first parameter - 'keyup' - string identifying an event
//second parameter - (event) - function that the listener will execute once the specified event is trigggered

txtFirstName.addEventListener('keyup', (event) => {
	// innerHTML - property sets or returns the HTML content
	spanFullName.innerHTML = txtFirstName.value;
	// event.target - contains the element where the event happened
	console.log(event.target);
	console.log(event.target.value);
});

